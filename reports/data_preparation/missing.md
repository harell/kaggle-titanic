

# Missing Records

For imputation, there are three methods which currently implemented in `caret` package:

1. k-nearest neighbors takes a sample with missing values and finds the k closest samples in the training set. The average of the k training set values for that predictor are used as a substitute for the original data. When calculating the distances to the training set samples, the predictors used in the calculation are the ones with no missing values for that sample and no missing values in the training set.

2. Fit a bagged tree model for each predictor using the training set samples. This is usually a fairly accurate model and can handle missing values. When a predictor for a sample requires imputation, the values for the other predictors are fed through the bagged tree and the prediction is used as the new value. This model can have significant computational cost.

3. The median of the predictor's training set values can be used to estimate the missing data.

## Missing Ages



Let's, tackle those missing ages. A common approach to this type of situation is to replacing the missings with the _average_ or the _median_ of the available values. In this case, that would mean replacing 177 missing Age values with 29.7 or 28 respectively.


```r
pander(summary(raw.train$Age))
```



|  Min.  |  1st Qu.  |  Median  |  Mean  |  3rd Qu.  |  Max.  |  NA's  |
|:------:|:---------:|:--------:|:------:|:---------:|:------:|:------:|
|  0.42  |   20.1    |    28    |  29.7  |    38     |   80   |  177   |

Taking that approach would be fine if only a small fraction of the ages were missing. However, with missings accounting for 20 percent of all Age data in a relatively small data set (<900 records), one could justify a search for a more refined method of imputation. Let's peek again at the list of currently available features:


```r
names(raw.train)
```

```
##  [1] "PassengerId" "Survived"    "Pclass"      "Name"        "Sex"        
##  [6] "Age"         "SibSp"       "Parch"       "Ticket"      "Fare"       
## [11] "Cabin"       "Embarked"
```

**PassengerId** is merely a record number, and we already know that splitting the ages solely by **Survived** doesn't reveal much. 

A boxplot of ages by **Pclass** (passenger traveling class) looks interesting...


```r
ggplot(raw.train, aes(Pclass, Age, fill=Pclass)) +
        geom_boxplot(outlier.size = 3) + coord_flip() + 
        ggtitle("Passenger Traveling Class by Age")
```

![plot of chunk PassengerId_boxplot](figure/PassengerId_boxplot1.png) 

```r
ggplot(raw.train, aes(x=Age, fill=Pclass, colour=Pclass)) + 
        geom_density(alpha=.5) + 
        facet_grid(Pclass~., scales="free") 
```

![plot of chunk PassengerId_boxplot](figure/PassengerId_boxplot2.png) 

This makes intuitive sense: Passengers in the upper classes (first and second) would tend to be wealthier, and in that period of U.S. history, acquiring wealth usually required a good deal of time (no dot-com kings in their 20s were aboard the Titanic on her maiden voyage). There are no missing values in **Pclass**, so we could replace the missing age for, say, a third class passenger with the _median_ or _average_ of the available ages for those in `Pclass="3"`. Doing so would be an improvement over assigning 29.7 to all **Age** missings.

Inspection of the next feature **Name** reveals what could be an even better approach...


```r
pander(head(raw.train$Name,n=10))
```

_Braund, Mr. Owen Harris_, _Cumings, Mrs. John Bradley (Florence Briggs Thayer)_, _Heikkinen, Miss. Laina_, _Futrelle, Mrs. Jacques Heath (Lily May Peel)_, _Allen, Mr. William Henry_, _Moran, Mr. James_, _McCarthy, Mr. Timothy J_, _Palsson, Master. Gosta Leonard_, _Johnson, Mrs. Oscar W (Elisabeth Vilhelmina Berg)_ and _Nasser, Mrs. Nicholas (Adele Achem)_

Notice the titles: _Mr._, _Mrs._, _Miss._, _Master._ following each of the surnames. The [Wikipedia entry](https://en.wikipedia.org/wiki/Master_(form_of_address/)) for the English honorific "Master" explains that,

> By the late 19th century, etiquette dictated that men be addressed as Mister, and boys as Master.

The title _Miss_ should help with differentiation betweeen younger and older females. Also, note the way the title appears in the name: The format "Surname, Title. Firstname..." is consistent in Name across all records. We use that pattern to create a custom function which employs a regular expression and the regexpr function to extract the title from each name:


```r
## function for extracting honorific (i.e. title) from the Name feature
getTitle <- function(data) {
        title.dot.start <- regexpr("\\,[A-Z ]{1,20}\\.", data$Name, TRUE)
        title.comma.end <- title.dot.start +
        attr(title.dot.start, "match.length")-1
        data$Title <- substr(data$Name, title.dot.start+2, title.comma.end-1)
        return (data$Title)
        }   
```

Let's fetch the titles, given them their own column in the pre.train data frame, and look at the uniques.


```r
pre.train$Title <- getTitle(raw.train)
unique(pre.train$Title)
```

```
##  [1] "Mr"           "Mrs"          "Miss"         "Master"      
##  [5] "Don"          "Rev"          "Dr"           "Mme"         
##  [9] "Ms"           "Major"        "Lady"         "Sir"         
## [13] "Mlle"         "Col"          "Capt"         "the Countess"
## [17] "Jonkheer"
```

To identify the titles which have at least one record with an age missing, We'll use the bystats function from the `Hmisc` package.


```r
options(digits=2)
require(Hmisc)
bystats(pre.train$Age, pre.train$Title,
        fun=function(x)c(Mean=mean(x),Median=median(x))) 
```

```
## 
##  c(4, 13, 4, 55, 13, 55, 4, 4) of pre.train$Age by pre.train$Title 
## 
##                N Missing Mean Median
## Capt           1       0 70.0   70.0
## Col            2       0 58.0   58.0
## Don            1       0 40.0   40.0
## Dr             6       1 42.0   46.5
## Jonkheer       1       0 38.0   38.0
## Lady           1       0 48.0   48.0
## Major          2       0 48.5   48.5
## Master        36       4  4.6    3.5
## Miss         146      36 21.8   21.0
## Mlle           2       0 24.0   24.0
## Mme            1       0 24.0   24.0
## Mr           398     119 32.4   30.0
## Mrs          108      17 35.9   35.0
## Ms             1       0 28.0   28.0
## Rev            6       0 43.2   46.5
## Sir            1       0 49.0   49.0
## the Countess   1       0 33.0   33.0
## ALL          714     177 29.7   28.0
```

Next, define the following custom function for imputing the missing ages:


```r
imputeMedian <- function(impute.var, filter.var, var.levels) {
        for (v in var.levels) {
                impute.var[ which( filter.var == v)] <- impute(impute.var[ 
                        which( filter.var == v)])
                }
        return (impute.var)
        }
```

we apply the impute function from the `Hmisc` package on a per-title basis to assign the median of the available ages to the missing age(s).


```r
set.caption("Before Imputation; How many values are NA (annotated by TRUE)?")
pander(table(is.na(pre.train$Age)))
```



|  FALSE  |  TRUE  |
|:-------:|:------:|
|   714   |  177   |

Table: Before Imputation; How many values are NA (annotated by TRUE)?


```r
pre.train$Age <- imputeMedian(pre.train$Age,
                              pre.train$Title,
                              c("Dr", "Master", "Mrs", "Miss", "Mr"))
```

After doing the age imputations, we check the **Age** data, and find that the function seems to have done its job.


```r
set.caption("Afrer Imputation; How many values are NA (annotated by TRUE)?")
pander(table(is.na(pre.train$Age)))
```



|  FALSE  |
|:-------:|
|   891   |

Table: Afrer Imputation; How many values are NA (annotated by TRUE)?

## Missing Embarked records



The **Embarked** feature has 2 missing values. Therefor, it should be fine to replace those missings with "Southampton", the most common value.


```r
pre.train$Embarked[which(is.na(raw.train$Embarked))] <- "Southampton"
```


