

# Irregular Records

## Irregular Fare records

While there are no missing **Fare** values, a summary does show the following


```r
pander(summary(pre.train$Fare))
```



|  Min.  |  1st Qu.  |  Median  |  Mean  |  3rd Qu.  |  Max.  |
|:------:|:---------:|:--------:|:------:|:---------:|:------:|
|   0    |   7.91    |   14.5   |  32.2  |    31     |  512   |

* That exceptionally high fare of $512.30 suggests that some tickets were purchased in groups; We'll address that later
* A zero fare might have been assigned to a baby. However, a closer look at records where _Fare = 0_ suggests otherwise...


```r
options(digits=2)
panderOptions("table.style","rmarkdown")
set.caption('Low Fare Values Tables')
pre.train.subset <- subset(pre.train, Fare < 7)
pander(pre.train.subset[order(pre.train.subset$Fare,pre.train.subset$Pclass), 
                 c("Age", "Title", "Pclass", "Fare")])
```



|  &nbsp;   |  Age  |  Title   |  Pclass  |  Fare  |
|:---------:|:-----:|:--------:|:--------:|:------:|
|  **264**  |  40   |    Mr    |  First   |   0    |
|  **634**  |  30   |    Mr    |  First   |   0    |
|  **807**  |  39   |    Mr    |  First   |   0    |
|  **816**  |  30   |    Mr    |  First   |   0    |
|  **823**  |  38   | Jonkheer |  First   |   0    |
|  **278**  |  30   |    Mr    |  Second  |   0    |
|  **414**  |  30   |    Mr    |  Second  |   0    |
|  **467**  |  30   |    Mr    |  Second  |   0    |
|  **482**  |  30   |    Mr    |  Second  |   0    |
|  **675**  |  30   |    Mr    |  Second  |   0    |
|  **733**  |  30   |    Mr    |  Second  |   0    |
|  **180**  |  36   |    Mr    |  Third   |   0    |
|  **272**  |  25   |    Mr    |  Third   |   0    |
|  **303**  |  19   |    Mr    |  Third   |   0    |
|  **598**  |  49   |    Mr    |  Third   |   0    |
|  **379**  |  20   |    Mr    |  Third   | 4.013  |
|  **873**  |  33   |    Mr    |  First   |   5    |
|  **327**  |  61   |    Mr    |  Third   | 6.237  |
|  **844**  | 34.5  |    Mr    |  Third   | 6.438  |
|  **819**  |  43   |    Mr    |  Third   |  6.45  |
|  **203**  |  34   |    Mr    |  Third   | 6.496  |
|  **372**  |  18   |    Mr    |  Third   | 6.496  |
|  **144**  |  19   |    Mr    |  Third   |  6.75  |
|  **655**  |  18   |   Miss   |  Third   |  6.75  |
|  **412**  |  30   |    Mr    |  Third   | 6.858  |
|  **826**  |  30   |    Mr    |  Third   |  6.95  |
|  **130**  |  45   |    Mr    |  Third   | 6.975  |
|  **805**  |  27   |    Mr    |  Third   | 6.975  |

Table: Low Fare Values Tables

The jump in fares from 0 to the 4-7 range suggests errors. We replace the zero **Fare** values with the median fare from the respective passenger class (**Pclass**) using the _imputMedian_ function introduced earlier.


```r
pre.train$Fare[which(pre.train$Fare == 0)] <- NA
pre.train$Fare <- imputeMedian(pre.train$Fare,
                               pre.train$Pclass,
                               c("First","Second","Third"))
```


