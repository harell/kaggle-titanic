```{r global_options, echo=FALSE}
opts_chunk$set(fig.width=12, fig.height=6,
               warning=FALSE, message=FALSE)
```

# Data Preparation / Data Munging

It's a good practice to leave the raw data untouched and make changes on a copy of it when possiable (i.e. when we can allocate the resources for utilizing that practice).

```{r, echo=FALSE}
pre.train <- raw.train
pre.test  <- raw.test
```


