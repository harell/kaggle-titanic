# Summary

* [Competition Details](competition_details/intro.md)
* [Get the data](get_the_data/intro.md)
* [Data Understanding](data_understanding/intro.md)
   * [Typical Methods](data_understanding/typical_methods.md)
   * [Custom Methods](data_understanding/custom_methods.md)
* [Data Preparation](data_preparation/intro.md)
   * [Missing Records](data_preparation/missing.md)
   * [Irregular Records](data_preparation/irregular.md)
   * [Feature Engineering](data_preparation/feature_engineering.md)   
* [Modelling](modelling/intro.md)
   * [Logistic Regression](modelling/logistic_regression.md)
   * [Random Forest](modelling/random_forest.md)
   * [SVM](modelling/svm.md)
* [Evaluation](evaluation/intro.md)

