

# Get the data

## Download the files

In this case, you need to download the train and test files manually from [Kaggle website](http://www.kaggle.com/c/titanic-gettingStarted/data), then:

* Place them in a directory called _data_.
* Make sure the files name are _train.csv_ and _test.csv_.



## Set variables (columns) type for train and test set

* Specifying missing types up front should make the data munging process a bit easier
* Specifying class definitions should be most conducive to modeling later.


```r
missing.types <- c("NA", "")
train.column.types <- c('integer',   # PassengerId
                        'factor',    # Survived 
                        'factor',    # Pclass
                        'character', # Name
                        'factor',    # Sex
                        'numeric',   # Age
                        'integer',   # SibSp
                        'integer',   # Parch
                        'character', # Ticket
                        'numeric',   # Fare
                        'character', # Cabin
                        'factor'     # 
)
test.column.types <- train.column.types[-2]    # no Survived column in test.csv
```

## Read the (raw) data


```r
raw.train <- read.csv(train, colClasses=train.column.types, 
                      na.strings=missing.types)
raw.test  <- read.csv(test, colClasses=test.column.types, 
                      na.strings=missing.types)
```

## Make the factors levels more informative


```r
x <- c("Survived",
       "Pclass",
       "Embarked")
from <- list(c("0","1"),
             c("1", "2", "3"),
             c("C", "Q", "S"))
to   <- list(c("Perished","Survived"),
             c("first", "second", "third"),
             c("Cherbourg", "Queenstown", "Southampton"))
        
level_mapping <- function(draft.dataset,label.info){
        for (i in 1:length(label.info[[1]])){
                draft.dataset[[label.info[[1]][[i]]]] <- mapvalues(
                        draft.dataset[[label.info[[1]][[i]]]],
                        from = label.info[[2]][[i]],
                        to = label.info[[3]][[i]])
                } 
        return(draft.dataset)
        }

label.info <- list(x,from,to)
raw.train  <- level_mapping(raw.train,label.info)

label.info <- list(x[-1],from[-1],to[-1]) # no Survived variable in test.csv
raw.test   <- level_mapping(raw.test,label.info)
```

---

