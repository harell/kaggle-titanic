The following describes an approach to the [Titanic survival prediction challenge](http://www.kaggle.com/c/titanic-gettingStarted) presented by Kaggle. 

This project intent to generlize common notions:

1. Working in a standard process (in this case, it was derived form the `CRISP-DM` frame work)
2. Authoring dynamic documents and reports from R which are fully reproducible (by using GitBook).
3. To demonstrate utilization of some of the tools of the data science trade, especially the `caraet` package.

This project was inspired from [*wehrley*](https://github.com/wehrley/wehrley.github.io/blob/master/SOUPTONUTS.md) work.

