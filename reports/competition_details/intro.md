# Competition Details

In this challenge, Kaggle ask to complete the analysis of what sorts of people were likely to survive. In particular, Kaggle ask to apply the tools of machine learning to predict which passengers survived the tragedy.

![Titanic Blueprint](figure/Titanic-Blueprint.png) 
