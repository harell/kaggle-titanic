

# Custom Methods

## Structure of Family Trees

The structure of relationships between individuals, suggested by [Matt Hagy](http://www.kaggle.com/c/titanic-gettingStarted/prospector#208), may provide valuable information for predicting survival. As discussed below, these relationships can be constructed from the given data. The following figures show the structure of close relations (spouses and parent/child) within the training and test data (there are relationships between training and test individuals and these may be particularly useful).

![1st class families](figure/9S9Ab.png) 

![2nd class families](figure/xmBa3.png) 

![3rd class families](figure/hIJJU.png) 

The markers represent individuals with females shown as diamonds and males as circles. Maker color denotes survival: green - survived, red - died, black - unknown (individual from the test set). The lines represent the structure of the relationships. Vertical lines join parent/child relations with the parent(s) always on top. Husband/wife relationships are shown by horizontal lines, as are sibling relationships. Individual graphs having only horizontal lines denote spouse relationships. There do exist a few sibling only relationships, lacking parents, but these are not shown in the current figures. Additionally, extended relationships have also been found, which could be cousins, aunts, or distant relatives, and these too are not shown in the current figures.

The relationships are constructed from the given data using only simple heuristics based around satisfying constraints. The heuristics only involve 2 adjustable parameters: 

* minimum age for marriage and 
* minimum age to give birth 

Both parameters have been set to 14. The remaining constraints are based around last names, maiden names, titles (Miss. vs. Mrs.), pclass, embarked, parch, and sibsp attributes, and known ages.
