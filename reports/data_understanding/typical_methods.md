

# Typical Methods

We start by looking at `missing` data in the training set. We'll plot a missingness map showing where missingness occurs in the train set.


```r
## map missing data by provided feature
require(Amelia)
missmap(raw.train[-1], main="Titanic Training Data - Missings Map", 
        col=c("yellow", "black"), legend=TRUE)
```

![plot of chunk Missings_Map](figure/Missings_Map.png) 

Roughly 20 percent of the Age data is missing, and well above 70 percent of the passengers cannot be linked to a specific cabin number. While the proportion of Age "missings" is likely small enough for reasonable replacement with some form of imputation, the cabin missings seem too extensive to make reliable imputation possible. Nevertheless, _some_ data could be better than _zero_ data, so we'll look at cabin numbers later to see how we can put them to use.

Before we start filling in missing data, let's see what can be learned from the data we have. Putting some simple data visualization tools to work can take us a long way toward understanding what might influence the outcome we're trying to predict - in this case, whether or not a passenger survived. Below are some graphs segmented by the `variable type`; discrete or continuous.

## Variables frequencies visualization

### Discrete variables - barplots


```r
## 1. Reshape the data
raw.train.discrete.molten <- melt(raw.train, id="Survived",
                         measure.vars = c("Survived",
                                          "Pclass",
                                          "Sex",
                                          "SibSp",
                                          "Parch",
                                          "Embarked"), 
                         variable.name = "Variable", na.rm=TRUE)

## 2. Set bargraphs titles
bar.titles <- c("Survived\n(passenger fate)",
                "Pclass\n(passenger traveling class)",
                "Sex\n(gender)",
                "SibSp\n(siblings + spouse aboard)",
                "Parch\n(parents + kids aboard)",
                "Embarked\n(port of embarkation)")

bar_labeller <- function(variable,value){
  return(bar.titles[value])}

## 3. Plot the melted data
ggplot(raw.train.discrete.molten, aes(value)) + 
        geom_bar(position="dodge",fill="#7CAE00") +
        facet_grid(.~Variable, scales="free", labeller=bar_labeller) +
        theme(axis.text.x = element_text(angle=30, vjust=1)) + xlab("")
```

![plot of chunk Discrete_data_visualization ](figure/Discrete_data_visualization .png) 

### Continuous variables - histograms


```r
## 1. Reshape the data
raw.train.continuous.molten <- melt(raw.train, id="Survived",
                         measure.vars = c("Age",
                                          "Fare"), 
                         variable.name = "Variable", na.rm=TRUE)

## 2. Set histograms titles
bar.titles <- c("Age",
                "Fare (fee paid for ticket[s])")

bar_labeller <- function(variable,value){
  return(bar.titles[value])}

## 3. Plot the melted data
ggplot(raw.train.continuous.molten, aes(value)) + 
        geom_histogram(binwidth = 4, fill="orange") +
        facet_grid(Variable~., scales="free", labeller=bar_labeller) +
        scale_x_continuous(limits=c(0, 100), breaks=seq(0,100,by=5)) + xlab("") 
```

![plot of chunk data_visualization ](figure/data_visualization .png) 

Some exploratory data analysis obseervations; note the dominant categories in the first three graphs:

* more passengers perished than survived
* about twice as many passengers in 3rd class than in either 1st or 2nd
* male passengers far outnumbered females

Perhaps these are the first clues that the two themes discussed earlier - women and children first policy, and location on the ship - could dictate the feature set. Although the fact that Southampton was the port of embarkation for most passengers doesn't make for a very balanced Embarked factor, it might mean something in the final analysis

## Discrete variables stratified by passenger fate

There at least two ways to summarize the discrete variables; 

* numeric summary, e.g. contingency table
* graphic summary, e.g. stratified bargraphs and mosaic plots

### Passenger Fate by Traveling Class 

To get a hunch on the traveling class influence on the odds of a passenger's survival, we build a contingency table.


```r
panderOptions('table.split.table', Inf)
panderOptions("table.style","rmarkdown")

table <- with(raw.train,table(Pclass,Survived))
set.caption("Contingency table")
pander(table)
```



|    &nbsp;    |  Perished  |  Survived  |
|:------------:|:----------:|:----------:|
|  **First**   |     80     |    136     |
|  **Second**  |     97     |     87     |
|  **Third**   |    372     |    119     |

Table: Contingency table

```r
table <- with(raw.train,table(Pclass,Survived))
set.caption("Proportions table")
pander(round(prop.table(table,1),2))
```



|    &nbsp;    |  Perished  |  Survived  |
|:------------:|:----------:|:----------:|
|  **First**   |    0.37    |    0.63    |
|  **Second**  |    0.53    |    0.47    |
|  **Third**   |    0.76    |    0.24    |

Table: Proportions table

Mosaic plots offer an interesting - and arguably under-utilized-way to summarize data. The area of the bar
in a mosaic plot expresses the size of the group.


```r
require(vcd)
mosaicplot(raw.train$Pclass ~ raw.train$Survived, 
           main="Passenger Fate by Traveling Class", shade=FALSE, 
           color=TRUE, xlab="Pclass", ylab="Survived")
```

![plot of chunk traveling_class_Mosaic_plots](figure/traveling_class_Mosaic_plots.png) 

The tables and the mosaic plot, suggest that traveling class did influence the odds of a passenger's survival, the higher the socio-economic status, the higher the odd of a person to survive.

---

### Passenger Fate by Gender 

Do you recall the earlier bar graph showing that some 2/3 of passengers were males? That is taken into account by the width of the two rectangles labeled "male" in the mosaic below. Now look at the height of the leftmost light gray rectangle [representing the proportion of females who survived] and compare it to the much shorter light gray rectange [representing proportion of males who survived]. Gender should certainly prove to be a prominent feature in the final model.


```r
mosaicplot(raw.train$Sex ~ raw.train$Survived, 
           main="Passenger Fate by Gender", shade=FALSE, color=TRUE, 
           xlab="Sex", ylab="Survived")
```

![plot of chunk Gender_Mosaic_plots](figure/Gender_Mosaic_plots.png) 

---

### Passenger Fate by Port of Embarkation 

While passenger survival didn't vary as much across the three ports of embarkation as it did between genders and traveling classes, perhaps the Embarked feature will prove useful at some point.


```r
mosaicplot(raw.train$Embarked ~ raw.train$Survived, 
           main="Passenger Fate by Port of Embarkation",
           shade=FALSE, color=TRUE, xlab="Embarked", ylab="Survived")
```

![plot of chunk Embarkation_Mosaic_plots](figure/Embarkation_Mosaic_plots.png) 


## Continuous variables stratified by passenger fate

### Passenger Fate by Age 

Is it possible that "survival of the fittest" dictated the fate of passengers in certain parts of the ship? Perhaps, though it isn't apparent at first glance from the boxplot of Age by Survival.


```r
ggplot(raw.train, aes(Survived, Age, fill=Survived)) +
        geom_boxplot(outlier.size = 3) + coord_flip()
```

![plot of chunk AGE_boxplot](figure/AGE_boxplot1.png) 

```r
ggplot(raw.train, aes(x=Age, fill=Survived, colour=Survived)) + geom_density(alpha=.5) + 
        facet_grid(Survived~., scales="free") 
```

![plot of chunk AGE_boxplot](figure/AGE_boxplot2.png) 

The box plots (which depict the _mean_, _median_ and other _quantiles_) and density plots of the stratified variable, appear to be similar, with a slight difference  in the left tail. 

### Passenger Fate by Fare

For this variable, we only make the following notion. Although fare and class are closely related, it might be worth throwing the Fare feature into the mix as another way to define a passenger's location on the ship.

## [Correlogram](https://en.wikipedia.org/wiki/Correlogram)

The correlogram(s) shown below confirms a couple of observations already made

* Survival odds drop with class

* Age may not prove to be a significant predictor.

* Given that the upper class ranks tend to be represented by an older demographic, an inverse correlation between age and traveling class is to be expected.


```r
library(ellipse)
corrgram.data <- data.frame(Survived=as.numeric(raw.train$Survived), 
                            Pclass=as.numeric(raw.train$Pclass),
                            Age=raw.train$Age,
                            SibSp=as.numeric(raw.train$SibSp),
                            Parch=as.numeric(raw.train$Parch),
                            Fare=raw.train$Fare)

correlation <- cor(corrgram.data[complete.cases(corrgram.data),])
plotcorr(correlation, main="Simple correlation matrix")
```

![plot of chunk Correlogram](figure/Correlogram1.png) 

```r
library(corrgram)
corrgram(corrgram.data, order=TRUE, lower.panel=panel.ellipse,
         upper.panel=panel.pts, text.panel=panel.txt,
         diag.panel=panel.minmax, 
         main="Advance correlation matrix")
```

![plot of chunk Correlogram](figure/Correlogram2.png) 

## Basic Classification Tree

Decision trees provide a convenient and efficient representation of knowledge. We shall build a tree without the variables which contain strings, that is **Name**, **Ticket** and **Cabin**.


```r
require(party)
formula <- as.formula(Survived ~.)
tree.data <- ctree(formula, data = raw.train[,c(-1,-4,-9,-11)])
plot(tree.data, type="simple")
```

![plot of chunk Decision_trees_plot](figure/Decision_trees_plot.png) 
