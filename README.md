To load the project, you'll first need to `setwd()` into the directory
where this README file is located. Then you need to run the following two
lines of R code:

```
library('ProjectTemplate')
load.project()
```

After you enter the second line of code, type the following lines to create reports.

```
## Create the GitBook from the SUMMMARY
generateGitBook(dir="reports")
## Convert .Rmd files to .md files
knitGitBook(dir="reports")
```

